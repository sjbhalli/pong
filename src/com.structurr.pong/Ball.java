package com.structurr.pong;

import android.content.Context;

import java.util.Random;

/**
 * Created by Sam on 5/31/2014.
 */
public class Ball extends MainActivity {

    int interval = 0; // Value should be unchanged after initialization

    int ballAndPadCollisionVibrateInterval = 15;
    int ballAndEdgeCollisionVibrateInterval = 2;
    int gameOverVibrateInterval = 750;

    Pads pads = new Pads();

    Context currentContext;

    public void process() {
        new Thread(new Runnable() { // Do work on ball thread
            public void run() {
                getPosition();

                checkForCollisions();
                checkForGameOver();
            }
        }).start();
    }

    private void getPosition() { // Update ball position using speeds in Y and X
        ballPosX += ballSpeedX;
        ballPosY += ballSpeedY;
    }

    private void checkForCollisions() { // Check for collisions between ball and pads and canvas edge
        BallPadCollisionSoundPlayerTask beepTask = new BallPadCollisionSoundPlayerTask();

        //Check for collision between ball and top pad
        if (ballPosX + ballHeightWidth > topPadPos && ballPosX < topPadPos + padWidth &&
                ballPosY <= 0 + padding + padHeight && ballPosY >= 0 + (padding / 2)) {
            beepTask.execute();

            setSpeed();
            ballSpeedY *= -1;

            ballSpeedX += (((ballPosX + ballHeightWidth) - topPadPos) / (padWidth / 20) - 10);

            vibrator.vibrate(ballAndPadCollisionVibrateInterval * hapticMultiplier);
        }

        // Check for collision between ball and bottom pad
        if (ballPosX + ballHeightWidth > bottomPadPos && ballPosX < bottomPadPos + padWidth &&
                ballPosY + ballHeightWidth >= canvasHeight - padding - padHeight &&
                ballPosY + ballHeightWidth <= canvasHeight - (padding / 2)) {
            beepTask.execute();

            setSpeed();
            ballSpeedY *= -1;

            ballSpeedX += (((ballPosX + ballHeightWidth) - bottomPadPos) / (padWidth / 20) - 10);

            vibrator.vibrate(ballAndPadCollisionVibrateInterval * hapticMultiplier);
        }

        // Check for collision between ball and canvas edges
        if (ballPosX < 0 || ballPosX > canvasWidth - ballHeightWidth) {
            ballSpeedX *= -1;

            vibrator.vibrate(ballAndEdgeCollisionVibrateInterval * hapticMultiplier);
        }
    }

    private void checkForGameOver() { // Check for collision between ball and game over area
        if (ballPosY + ballHeightWidth > canvasHeight - padding - padHeight
                && (ballPosX + ballHeightWidth < bottomPadPos
                || ballPosX > bottomPadPos + padWidth)) {
//            pads.resetPositions();
            if (isRunning) topPlayerScore += 1;
            isRunning = false;
            vibrator.vibrate(gameOverVibrateInterval * hapticMultiplier);
        } else if (ballPosY < 0 + padding + padHeight
                && (ballPosX + ballHeightWidth < topPadPos || ballPosX > topPadPos + padWidth)) {
//            pads.resetPositions();
            if (isRunning) bottomPlayerScore += 1;
            isRunning = false;
            vibrator.vibrate(gameOverVibrateInterval * hapticMultiplier);
        }
    }

    private void setSpeed() { // Set ball speed in Y and X
        setSpeedY();
        setSpeedX();
    }

    private void setSpeedY() {
        if (ballSpeedY < 0) {
            ballSpeedY -= canvasWidth / 480;
        } else if (ballSpeedY > 0) {
            ballSpeedY += canvasWidth / 480;
        }
    }

    private void setSpeedX() { // Generate random ball speed in X
        Random random = new Random();
        int rand = 0;

        // Generate random speed
        if (ballSpeedY < 0) {
            rand = random.nextInt(-ballSpeedY + 2);
            while (rand < -ballSpeedY - 7 || rand == 0) {
                rand = random.nextInt(-ballSpeedY + 2);
            }
        } else if (ballSpeedY > 0) {
            rand = random.nextInt(ballSpeedY + 2);
            while (rand < ballSpeedY - 7 || rand == 0) {
                rand = random.nextInt(ballSpeedY + 2);
            }
        }

        // Generate direction
        if (ballSpeedX < 0) {
            ballSpeedX = rand * -1;
        } else if (ballSpeedX > 0) {
            ballSpeedX = rand;
        } else {
            ballSpeedX = rand * -1;
        }
    }

    public void reset() { // Reset ball to original position and reset ball speeds
        ballPosX = (canvasWidth / 2) - (ballHeightWidth / 2);
        ballPosY = (canvasHeight / 2) - (ballHeightWidth / 2);

        Random random = new Random();
        int direction = random.nextInt(2);

        if (bottomPlayerScore - topPlayerScore >= 3)
            direction = 0;

        // Generate random direction
        if (direction == 0) {
            ballSpeedY = 5 + initialBallSpeedDifference;
        } else if (direction == 1) {
            ballSpeedY = -5 - initialBallSpeedDifference;
        }

        setSpeedX();
    }

    public void setInitialSpeedY() { // Set initial ball speed in Y
        Random random = new Random();
        int direction = random.nextInt(2);

        if (direction == 0) {
            ballSpeedY = 5 + initialBallSpeedDifference;
        } else if (direction == 1) {
            ballSpeedY = -5 - initialBallSpeedDifference;
        }
    }

    public void setInitialSpeedX() { // Generate random ball speed in X
        Random random = new Random();

        // Generate random speed
        int randSpeed = random.nextInt(7);
        while (randSpeed < 3) {
            randSpeed = random.nextInt(7);
        }

        // Generate random direction
        int randDirection = random.nextInt(2);
        if (randDirection == 0) {
            ballSpeedX = randSpeed * (canvasWidth / 480);
        } else if (randDirection == 1) {
            ballSpeedX = randSpeed * (canvasWidth / 480) * -1;
        }
    }

    public void setContext(Context context) {
        currentContext = context;
    }

}