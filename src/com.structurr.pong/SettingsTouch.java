package com.structurr.pong;

import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by Sam on 6/24/2014.
 */
public class SettingsTouch extends SettingsView {

    static int downPositionX = 0;
    static int downPositionY = 0;
    static int movePositionX = 0;
    static int movePositionY = 0;
    static int upPositionX = 0;
    static int upPositionY = 0;

    Context currentContext;

    public void process(final MotionEvent motionEvent) {
        final Preferences preferences = new Preferences();

        int motionAction = (motionEvent.getAction() & MotionEvent.ACTION_MASK);

        switch (motionAction) {
            case MotionEvent.ACTION_DOWN:
                downPositionX = (int) motionEvent.getX();
                downPositionY = (int) motionEvent.getY();

                if (gameColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    gameColorOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (backColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    backColorOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (seizureModeToggleFinalBounds.contains(downPositionX, downPositionY)) {
                    seizureModeOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (audioOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    audioOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (hapticOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hapticOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (initialSpeedChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    initialBallSpeedOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                    resetButtonPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier * 2);
                }

                if (!gameColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    gameColorOptionPressed = false;
                }
                if (!backColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    backColorOptionPressed = false;
                }
                if (!seizureModeToggleFinalBounds.contains(downPositionX, downPositionY)) {
                    seizureModeOptionPressed = false;
                }
                if (!audioOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    audioOptionPressed = false;
                }
                if (!hapticOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hapticOptionPressed = false;
                }
                if (!initialSpeedChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    initialBallSpeedOptionPressed = false;
                }
                if (!resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                    resetButtonPressed = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                movePositionX = (int) motionEvent.getX();
                movePositionY = (int) motionEvent.getY();

                if (gameColorChangeOptionFinalBounds.contains(movePositionX, movePositionY) && gameColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    gameColorOptionPressed = true;
                } else if (backColorChangeOptionFinalBounds.contains(movePositionX, movePositionY) && backColorChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    backColorOptionPressed = true;
                } else if (seizureModeToggleFinalBounds.contains(downPositionX, downPositionY) && seizureModeToggleFinalBounds.contains(downPositionX, downPositionY)) {
                    seizureModeOptionPressed = true;
                } else if (audioOptionFinalBounds.contains(downPositionX, downPositionY) && audioOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    audioOptionPressed = true;
                } else if (hapticOptionFinalBounds.contains(downPositionX, downPositionY) && hapticOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hapticOptionPressed = true;
                } else if (initialSpeedChangeOptionFinalBounds.contains(movePositionX, movePositionY) && initialSpeedChangeOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    initialBallSpeedOptionPressed = true;
                } else if (resetButtonFinalBounds.contains(movePositionX, movePositionY) && resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                    resetButtonPressed = true;
                }

                if (!gameColorChangeOptionFinalBounds.contains(movePositionX, movePositionY)) {
                    gameColorOptionPressed = false;
                }
                if (!backColorChangeOptionFinalBounds.contains(movePositionX, movePositionY)) {
                    backColorOptionPressed = false;
                }
                if (!seizureModeToggleFinalBounds.contains(downPositionX, downPositionY)) {
                    seizureModeOptionPressed = false;
                }
                if (!audioOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    audioOptionPressed = false;
                }
                if (!hapticOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hapticOptionPressed = false;
                }
                if (!initialSpeedChangeOptionFinalBounds.contains(movePositionX, movePositionY)) {
                    initialBallSpeedOptionPressed = false;
                }
                if (!resetButtonFinalBounds.contains(movePositionX, movePositionY)) {
                    resetButtonPressed = false;
                }
                break;
            case MotionEvent.ACTION_UP:
                upPositionX = (int) motionEvent.getX();
                upPositionY = (int) motionEvent.getY();

                setNonePressed();

                if (gameColorChangeOptionFinalBounds.contains(downPositionX, downPositionY) && gameColorChangeOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    AmbilWarnaDialog colorDialog = new AmbilWarnaDialog(currentContext, gameColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog dialog,
                                         int colorFromDialog) {
                            seizureModeStateText = "OFF";
                            gameColor = colorFromDialog;
                            preferences.setContext(currentContext);
                            preferences.putInt("gameColorPref", gameColor);
                            preferences.putInt("chosenGameColorPref", gameColor);
                            backColor = preferences.getInt("chosenBackColorPref", backColor);
                            preferences.putInt("backColorPref", backColor);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog dialog) {

                        }
                    }
                    );
                    colorDialog.show();
                } else if (backColorChangeOptionFinalBounds.contains(downPositionX, downPositionY) && backColorChangeOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    AmbilWarnaDialog colorDialog = new AmbilWarnaDialog(currentContext, backColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog dialog,
                                         int colorFromDialog) {
                            seizureModeStateText = "OFF";
                            backColor = colorFromDialog;
                            preferences.setContext(currentContext);
                            preferences.putInt("backColorPref", backColor);
                            preferences.putInt("chosenBackColorPref", backColor);
                            gameColor = preferences.getInt("chosenGameColorPref", gameColor);
                            preferences.putInt("gameColorPref", gameColor);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog dialog) {

                        }
                    }
                    );
                    colorDialog.show();
                } else if (seizureModeToggleFinalBounds.contains(downPositionX, downPositionY) && seizureModeToggleFinalBounds.contains(upPositionX, upPositionY)) {
                    if (seizureModeStateText == "OFF") {
                        seizureModeStateText = "BW";
                    } else if (seizureModeStateText == "BW") {
                        seizureModeStateText = "FULL";
                    } else if (seizureModeStateText == "FULL") {
                        seizureModeStateText = "OFF";
                        preferences.setContext(currentContext);
                        gameColor = preferences.getInt("chosenGameColorPref", Color.rgb(255, 255, 255));
                        backColor = preferences.getInt("chosenBackColorPref", Color.rgb(0, 0, 0));
                    }
                    /*if (seizureModeStateText == "OFF") {
                        atSettings = false;
                        atSeizureConfirmation = true;
                    } else if (seizureModeStateText == "ON") {
                        seizureModeStateText = "OFF";
                    }*/
                } else if (audioOptionFinalBounds.contains(downPositionX, downPositionY) && audioOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    if (audioOptionStateText.equals("BOP")) {
                        audioOptionStateText = "BEEP";
                        soundPool.play(beepID, 1f, 1f, 1, 0, 1f);
                    } else if (audioOptionStateText.equals("BEEP")) {
                        audioOptionStateText = "OFF";
                    } else if (audioOptionStateText.equals("OFF")) {
                        audioOptionStateText = "BOP";
                        soundPool.play(bopID, 1f, 1f, 1, 0, 1f);
                    }

                    preferences.setContext(currentContext);
                    preferences.putString("audioOptionState", audioOptionStateText);
                } else if (hapticOptionFinalBounds.contains(downPositionX, downPositionY) && hapticOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    if (hapticOptionStateText.equals("LOW")) {
                        hapticOptionStateText = "HIGH";
                        hapticMultiplier = 2;
                        vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                    } else if (hapticOptionStateText.equals("HIGH")) {
                        hapticOptionStateText = "OFF";
                        hapticMultiplier = 0;
                    } else if (hapticOptionStateText.equals("OFF")) {
                        hapticOptionStateText = "LOW";
                        hapticMultiplier = 1;
                        vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                    }

                    preferences.setContext(currentContext);
                    preferences.putString("hapticOptionState", hapticOptionStateText);
                } else if (initialSpeedChangeOptionFinalBounds.contains(downPositionX, downPositionY) && initialSpeedChangeOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    if (initialBallSpeedDifference == 1) {
                        initialBallSpeedDifference = 2;
                        initialBallSpeedPrefText = "SLOW";
                    } else if (initialBallSpeedDifference == 2) {
                        initialBallSpeedDifference = 3;
                        initialBallSpeedPrefText = "NORMAL";
                    } else if (initialBallSpeedDifference == 3) {
                        initialBallSpeedDifference = 4;
                        initialBallSpeedPrefText = "FAST";
                    } else if (initialBallSpeedDifference == 4) {
                        initialBallSpeedDifference = 5;
                        initialBallSpeedPrefText = "SUPER FAST";
                    } else if (initialBallSpeedDifference == 5) {
                        initialBallSpeedDifference = 1;
                        initialBallSpeedPrefText = "SUPER SLOW";
                    }

                    preferences.setContext(currentContext);
                    preferences.putInt("initialBallSpeedDifference", initialBallSpeedDifference);
                    preferences.putString("initialBallSpeedPrefText", initialBallSpeedPrefText);
                } else if (resetButtonFinalBounds.contains(downPositionX, downPositionY) && resetButtonFinalBounds.contains(upPositionX, upPositionY)) {
                    preferences.setContext(currentContext);
                    preferences.putInt("gameColorPref", -1);
                    preferences.putInt("chosenGameColorPref", -1);
                    preferences.putInt("backColorPref", -16777216);
                    preferences.putInt("chosenBackColorPref", -16777216);
                    gameColor = -1;
                    backColor = -16777216;
                    seizureModeStateText = "OFF";
                    audioOptionStateText = "BOP";
                    hapticOptionStateText = "LOW";
                    hapticMultiplier = 0;
                    initialBallSpeedPrefText = "NORMAL";
                    initialBallSpeedDifference = 3;
                    preferences.putString("audioOptionState", audioOptionStateText);
                    preferences.putString("initialBallSpeedPrefText", "NORMAL");
                    preferences.putInt("initialBallSpeedDifference", 3);
                    toast.setText("Preferences reset");
                    toast.show();
                }
                break;
        }
    }

    private static void setNonePressed() {
        gameColorOptionPressed = false;
        backColorOptionPressed = false;
        seizureModeOptionPressed = false;
        audioOptionPressed = false;
        hapticOptionPressed = false;
        initialBallSpeedOptionPressed = false;
        resetButtonPressed = false;
    }

    public void setContext(Context context) {
        currentContext = context;
    }

}
