package com.structurr.pong;

import android.app.Notification;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Sam on 6/4/2014.
 */

public class IntroView extends MainActivity { // Draws splash screen

    static Rect easyOptionFinalBounds = new Rect();
    static Rect normalOptionFinalBounds = new Rect();
    static Rect hardOptionFinalBounds = new Rect();
    static Rect intellidroidOptionFinalBounds = new Rect();
    static Rect settingsOptionFinalBounds = new Rect();
    static Rect resetButtonFinalBounds = new Rect();

    static boolean easyOptionPressed = false;
    static boolean normalOptionPressed = false;
    static boolean hardOptionPressed = false;
    static boolean intellidroidOptionPressed = false;
    static boolean settingsOptionPressed = false;
    static boolean resetButtonPressed = false;

    public static class Draw extends View {

        boolean firstDraw = true;

        int textSize = 1;

        Rect gameNameBounds = new Rect(); // Used to determine how much space the game name will take up
        Rect devNameBounds = new Rect(); // Used to determine how much space the developer name will take up
        Rect easyOptionBounds = new Rect();
        Rect hardOptionBounds = new Rect();
        Rect normalOptionBounds = new Rect();
        Rect intellidroidOptionBounds = new Rect();
        Rect settingsOptionBounds = new Rect();
        Rect resetButtonBounds = new Rect();

        final String gameName = "PONG";
        final String devName = "STRUCTURR";
        final String easyOptionText = "EASY";
        final String normalOptionText = "NORMAL";
        final String hardOptionText = "HARD";
        final String intellidroidOptionText = "INTELLIDROID";
        final String settingsOptionText = "SETTINGS";
        final String resetOptionText = "RESET";

        int gameNameTop = 0;
        int gameNameLeft = 0;
        int devNameTop = 0;
        int devNameLeft = 0;
        int easyOptionTop = 0;
        int easyOptionLeft = 0;
        int normalOptionTop = 0;
        int normalOptionLeft = 0;
        int hardOptionTop = 0;
        int hardOptionLeft = 0;
        int intellidroidOptionTop = 0;
        int intellidroidOptionLeft = 0;
        int settingsOptionTop = 0;
        int settingsOptionLeft = 0;
        int resetButtonTop = 0;
        int resetButtonLeft = 0;

        Ball ball = new Ball(); // Instance of Ball class to use to set speeds

        public Draw(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            changeStatusBarColor();

            if (firstDraw) {
                setGlobalVars(canvas);
                firstDraw = false;
            }

            if (seizureModeStateText != "OFF") {
                MainActivity.generateColorsForSeizureMode();
            }

            drawBackground(canvas);
            drawGameName(canvas);
//            drawDevName(canvas);
            drawOptions(canvas);
            drawResetButton(canvas);

            invalidate(); // Draw everything again
        }

        private void drawBackground(Canvas canvas) { // Draw any elements that are considered background
            canvas.drawColor(backColor);
        }

        private void drawGameName(Canvas canvas) { // Draw the game name
            textPaint.setTypeface(customFace);
            textPaint.setTextSize(textSize);
            textPaint.setColor(gameColor);

            //TODO Make it only run once ^^

            // Puts gameNameBounds as an enclosing rectangle to determine the space taken up by the game name
            textPaint.getTextBounds(gameName, 0, gameName.length(), gameNameBounds);

            // While game name takes up less space than 80% of canvas, increase text size
            while (gameNameBounds.width() < canvasWidth * 0.80) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(gameName, 0, gameName.length(), gameNameBounds);
            }

            // While game name takes up more space than 85% of canvas, decrease text size
            while (gameNameBounds.width() > canvasWidth * 0.85) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(gameName, 0, gameName.length(), gameNameBounds);
            }

            gameNameTop = (int) ((canvasHeight * 0.2) + gameNameBounds.height());
            gameNameLeft = (canvasWidth / 2) - (gameNameBounds.width() / 2);

            canvas.drawText(gameName, gameNameLeft, gameNameTop, textPaint);

        }

        private void drawDevName(Canvas canvas) { // Draw the developer name

            // Puts devNameBounds as an enclosing rectangle to determine the space taken up by the developer name
            textPaint.getTextBounds(devName, 0, devName.length(), devNameBounds);

            // While developer name takes up less space than 20% of canvas, increase text size
            while (devNameBounds.width() < canvasWidth * 0.20) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(devName, 0, devName.length(), devNameBounds);
            }

            // While developer name takes up more space than 25% of canvas, decrease text size
            while (devNameBounds.width() > canvasWidth * 0.25) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(devName, 0, devName.length(), devNameBounds);
            }

            devNameTop = (int) ((canvasHeight * 0.2) + gameNameBounds.height() + (devNameBounds.height() + (devNameBounds.height() / 2)));
            devNameLeft = (canvasWidth / 2) - (devNameBounds.width() / 2);

            canvas.drawText(devName, devNameLeft, devNameTop, textPaint);
        }

        private void drawOptions(Canvas canvas) {
            // Process text for easy game mode
            textPaint.getTextBounds(easyOptionText, 0, easyOptionText.length(), easyOptionBounds);

            while (easyOptionBounds.height() < canvasWidth * 0.0425) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(easyOptionText, 0, easyOptionText.length(), easyOptionBounds);
            }

            while (easyOptionBounds.height() > canvasWidth * 0.045) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(easyOptionText, 0, easyOptionText.length(), easyOptionBounds);
            }

            // Process text for normal game mode
            textPaint.getTextBounds(normalOptionText, 0, normalOptionText.length(), normalOptionBounds);

            while (normalOptionBounds.height() < canvasWidth * 0.0425) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(normalOptionText, 0, normalOptionText.length(), normalOptionBounds);
            }

            while (normalOptionBounds.height() > canvasWidth * 0.045) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(normalOptionText, 0, normalOptionText.length(), normalOptionBounds);
            }

            // Process text for hard game mode
            textPaint.getTextBounds(hardOptionText, 0, hardOptionText.length(), hardOptionBounds);

            while (hardOptionBounds.height() < canvasWidth * 0.0425) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(hardOptionText, 0, hardOptionText.length(), hardOptionBounds);
            }

            while (hardOptionBounds.height() > canvasWidth * 0.045) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(hardOptionText, 0, hardOptionText.length(), hardOptionBounds);
            }

            // Process text for Intellidroid game mode
            textPaint.getTextBounds(intellidroidOptionText, 0, intellidroidOptionText.length(), intellidroidOptionBounds);

            while (intellidroidOptionBounds.height() < canvasWidth * 0.0425) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(intellidroidOptionText, 0, intellidroidOptionText.length(), intellidroidOptionBounds);
            }

            while (intellidroidOptionBounds.height() > canvasWidth * 0.045) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(intellidroidOptionText, 0, intellidroidOptionText.length(), intellidroidOptionBounds);
            }

            // Process text for setting menu
            textPaint.getTextBounds(settingsOptionText, 0, settingsOptionText.length(), settingsOptionBounds);
            
            while (settingsOptionBounds.height() < canvasWidth * 0.0425) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(settingsOptionText, 0, settingsOptionText.length(), settingsOptionBounds);
            }
            
            while (settingsOptionBounds.height() > canvasWidth * 0.045) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(settingsOptionText, 0, settingsOptionText.length(), settingsOptionBounds);                
            }

            easyOptionTop = (int) ((canvasHeight * 0.6));// + (easyOptionBounds.height()));
            easyOptionLeft = (canvasWidth / 2) - (easyOptionBounds.width() / 2);
            normalOptionTop = (int) (easyOptionTop + ((normalOptionBounds.height() * 1) + normalOptionBounds.height()));
            normalOptionLeft = (canvasWidth / 2) - (normalOptionBounds.width() / 2);
            hardOptionTop = (int) (normalOptionTop + (hardOptionBounds.height() * 1) + hardOptionBounds.height());
            hardOptionLeft = (canvasWidth / 2) - (hardOptionBounds.width() / 2);
            intellidroidOptionTop = (int) (hardOptionTop + (intellidroidOptionBounds.height() * 1) + intellidroidOptionBounds.height());
            intellidroidOptionLeft = (canvasWidth / 2) - (intellidroidOptionBounds.width() / 2);
            settingsOptionTop = (int) (intellidroidOptionTop + (2 * settingsOptionBounds.height()) + settingsOptionBounds.height());
            settingsOptionLeft = (canvasWidth / 2) - (settingsOptionBounds.width() / 2);

            // Draw all options text

            if (easyOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(easyOptionText, easyOptionLeft, easyOptionTop, textPaint);
            textPaint.setAlpha(255);

            if (normalOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(normalOptionText, normalOptionLeft, normalOptionTop, textPaint);
            textPaint.setAlpha(255);

            if (hardOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(hardOptionText, hardOptionLeft, hardOptionTop, textPaint);
            textPaint.setAlpha(255);

            if (intellidroidOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(intellidroidOptionText, intellidroidOptionLeft, intellidroidOptionTop, textPaint);
            textPaint.setAlpha(255);

            if (settingsOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(settingsOptionText, settingsOptionLeft, settingsOptionTop, textPaint);
            textPaint.setAlpha(255);

            setFinalOptionBounds();
        }

        private void drawResetButton(Canvas canvas) {
            textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);

            while (resetButtonBounds.height() < canvasHeight * 0.0125) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);
            }

            while (resetButtonBounds.height() > canvasHeight * 0.015) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);
            }

            resetButtonTop = (int) canvasHeight - (resetButtonBounds.height() / 2) - resetButtonBounds.height();
            resetButtonLeft  = (int) canvasWidth - resetButtonBounds.height() - resetButtonBounds.width();

            if (backColor > -1000000) {
                textPaint.setColor(Color.BLACK);
            } else if (backColor < -15777216) {
                textPaint.setColor(Color.WHITE);
            }

            if (resetButtonPressed) textPaint.setAlpha(50);
            canvas.drawText(resetOptionText, resetButtonLeft, resetButtonTop, textPaint);
            textPaint.setAlpha(255);

            setFinalOptionBounds();
            //TODO do only once
        }

        private void setFinalOptionBounds() {
            easyOptionFinalBounds.left = (int) (easyOptionLeft - (easyOptionBounds.height() * 0.5));
            easyOptionFinalBounds.top = (int) (easyOptionTop);
            easyOptionFinalBounds.right = (int) (easyOptionLeft + easyOptionBounds.width() + (easyOptionBounds.height() * 0.5));
            easyOptionFinalBounds.bottom = (int) (easyOptionTop + easyOptionBounds.height() + (easyOptionBounds.height() * 1.5));

            normalOptionFinalBounds.left = (int) (normalOptionLeft - (normalOptionBounds.height() * 0.5));
            normalOptionFinalBounds.top = (int) (normalOptionTop);
            normalOptionFinalBounds.right = (int) (normalOptionLeft + normalOptionBounds.width() + (normalOptionBounds.height() * 0.5));
            normalOptionFinalBounds.bottom = (int) (normalOptionTop + normalOptionBounds.height() + (normalOptionBounds.height() * 1.5));

            hardOptionFinalBounds.left = (int) (hardOptionLeft - (hardOptionBounds.height() * 0.5));
            hardOptionFinalBounds.top = (int) (hardOptionTop);
            hardOptionFinalBounds.right = (int) (hardOptionLeft + hardOptionBounds.width() + (hardOptionBounds.height() * 0.5));
            hardOptionFinalBounds.bottom = (int) (hardOptionTop + hardOptionBounds.height() + (hardOptionBounds.height() * 1.5));

            intellidroidOptionFinalBounds.left = (int) (intellidroidOptionLeft - (intellidroidOptionBounds.height() * 0.5));
            intellidroidOptionFinalBounds.top = (int) (intellidroidOptionTop);
            intellidroidOptionFinalBounds.right = (int) (intellidroidOptionLeft + intellidroidOptionBounds.width() + (intellidroidOptionBounds.height() * 0.5));
            intellidroidOptionFinalBounds.bottom = (int) (intellidroidOptionTop + intellidroidOptionBounds.height() + (intellidroidOptionBounds.height() * 1.5));

            settingsOptionFinalBounds.left = (int) (settingsOptionLeft - (settingsOptionBounds.height() * 0.5));
            settingsOptionFinalBounds.top = (int) (settingsOptionTop);
            settingsOptionFinalBounds.right = (int) (settingsOptionLeft + settingsOptionBounds.width() + (settingsOptionBounds.height() * 0.5));
            settingsOptionFinalBounds.bottom = (int) (settingsOptionTop + settingsOptionBounds.height() + (settingsOptionBounds.height() * 1.5));

            resetButtonFinalBounds.left = resetButtonLeft - (resetButtonBounds.height() / 2);
            resetButtonFinalBounds.top = resetButtonTop;
            resetButtonFinalBounds.right = resetButtonLeft + resetButtonBounds.width() + (resetButtonBounds.height() / 2);
            resetButtonFinalBounds.bottom = 2 * canvasHeight;
        }

        private void setGlobalVars(Canvas canvas) { // Set variables that need a canvas to be initialized
            // and may be used later by other classes
            canvasHeight = canvas.getHeight();
            canvasWidth = canvas.getWidth();
            padding = (int) (canvasHeight * 0.15);

            padWidth = (int) (canvasWidth * 0.175);
            padHeight = (int) (canvasHeight * 0.0125);
            ballHeightWidth = (int) (canvasWidth * 0.03);

            topPadPos = ((canvasWidth / 2) - (padWidth / 2));
            bottomPadPos = ((canvasWidth / 2) - (padWidth / 2));
            ballPosX = ((canvasWidth / 2) - (ballHeightWidth / 2));
            ballPosY = ((canvasHeight / 2) - (ballHeightWidth / 2));
            ball.setInitialSpeedY();
            ball.setInitialSpeedX();
        }

    }

}
