/**
 * NOTE: This class is for the sole purpose of experimenting and perfecting the onTouchEvent method.
 * Delete only after it has served its purpose.
 */

package com.structurr.pong;

import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Sam on 6/2/2014.
 */
public class TouchExperimental extends MainActivity {

    public static void process(MotionEvent motionEvent) {

        Pads pads = new Pads();

        final int MAX_TOUCH_COUNT = 10;
        int[] x = new int[MAX_TOUCH_COUNT];
        int[] y = new int[MAX_TOUCH_COUNT];

        int pointerIndex = motionEvent.getActionIndex();//((motionEvent.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT);
        int pointerId = motionEvent.getPointerId(0);
        int motionAction = motionEvent.getAction();//(motionEvent.getAction() & MotionEvent.ACTION_MASK);
        int pointCount = motionEvent.getPointerCount();

//        for (int i = 0; i < pointCount && i < MAX_TOUCH_COUNT; i++) {
//            int id = motionEvent.getPointerId(i);
            x[pointerId] = (int) motionEvent.getX(pointerIndex);
            y[pointerId] = (int) motionEvent.getY(pointerIndex);
//        }

//        if (isRunning) {
//            switch (motionAction) {
//                case MotionEvent.ACTION_DOWN:
//                    bottomPadPos = x[pointerId];
//                    Log.d("Bottom", "action_down");
////                    break;
//                case MotionEvent.ACTION_POINTER_DOWN:
//                    bottomPadPos = x[pointerId];
//                    Log.d("Bottom", "action_pointer_down");
////                    break;
//                case MotionEvent.ACTION_UP:
//                    bottomPlayerMoving = false;
//                    Log.d("Bottom", "action_up");
////                    break;
//                case MotionEvent.ACTION_POINTER_UP:
//                    bottomPlayerMoving = false;
//                    Log.d("Bottom", "action_pointer_up");
////                    break;
//                case MotionEvent.ACTION_MOVE:
//                    bottomPadPos = x[pointerId];
//                    Log.d("Bottom", "action_move");
////                    break;
//            }
//        }

        if (isRunning) {
            switch (motionAction) {
                case MotionEvent.ACTION_DOWN:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        initialTPTop = x[pointerId];
                        topPadPosOnDown = topPadPos;
                        topPlayerMoving = true;
                        Log.d("Top", "action_down");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        initialTPBottom = x[pointerId];
                        bottomPadPosOnDown = bottomPadPos;
                        bottomPlayerMoving = true;
                        Log.d("Bottom", "action_down");
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        initialTPTop = x[pointerId];
                        topPadPosOnDown = topPadPos;
                        topPlayerMoving = true;
                        Log.d("Top", "action_pointer_down");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        initialTPBottom = x[pointerId];
                        bottomPadPosOnDown = bottomPadPos;
                        bottomPlayerMoving = true;
                        Log.d("Bottom", "action_pointer_down");
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    bottomPlayerMoving = false;
                    topPlayerMoving = false;
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    bottomPlayerMoving = false;
                    topPlayerMoving = false;
                    break;
                case MotionEvent.ACTION_MOVE:
                    for (int n = 0; n < pointCount; n++) {
                        if (y[n] < (canvasHeight / 2)) {
                            currTPTop = x[n];
                            topPlayerMoving = true;
                            pads.process();
                            Log.d("Top", "action_move");
                        } if (y[n] > (canvasHeight / 2)) {
                            currTPBottom = x[n];
                            bottomPlayerMoving = true;
                            pads.process();
                            Log.d("Bottom", "action_move");
                        }
                    }
                    break;
                default:
                    bottomPlayerMoving = false;
                    topPlayerMoving = false;
            }
        }
    }

}
