package com.structurr.pong;

import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Sam on 6/28/2014.
 */
public class SeizureModeConfirmationTouch extends SeizureModeConfirmationView {

    int downPositionX = 0;
    int downPositionY = 0;
    int movePositionX = 0;
    int movePositionY = 0;
    int upPositionX = 0;
    int upPositionY = 0;

    public void process (MotionEvent motionEvent) {

        int motionAction = (motionEvent.getAction() & MotionEvent.ACTION_MASK);

        switch (motionAction) {
            case MotionEvent.ACTION_DOWN:
                downPositionX = (int) motionEvent.getX();
                downPositionY = (int) motionEvent.getY();

                if (okayOptionFinalBounds.contains(downPositionX, downPositionY)) {
                   okayOptionPressed = true;
                }
                if (cancelOptionFinalBounds.contains(downPositionX, downPositionY)){
                    cancelOptionPressed = true;
                }

                break;
            case MotionEvent.ACTION_MOVE:
                movePositionX = (int) motionEvent.getX();
                movePositionY = (int) motionEvent.getY();

                if (okayOptionFinalBounds.contains(movePositionX, movePositionY) && okayOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    okayOptionPressed = true;
                }
                if (cancelOptionFinalBounds.contains(movePositionX, movePositionY) && cancelOptionFinalBounds.contains(downPositionX, downPositionY)){
                    cancelOptionPressed = true;
                }

                if (!okayOptionFinalBounds.contains(movePositionX, movePositionY) && okayOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    okayOptionPressed = false;
                }
                if (!cancelOptionFinalBounds.contains(movePositionX, movePositionY) && cancelOptionFinalBounds.contains(downPositionX, downPositionY)){
                    cancelOptionPressed = false;
                }

                break;
            case MotionEvent.ACTION_UP:
                upPositionX = (int) motionEvent.getX();
                upPositionY = (int) motionEvent.getY();

                setNonePressed();

                if (okayOptionFinalBounds.contains(downPositionX, downPositionY) && okayOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    Log.d("hello","test");
                    seizureModeStateText = "ON";
                    onBackPressed();
                } else if (cancelOptionFinalBounds.contains(downPositionX, downPositionY) && cancelOptionFinalBounds.contains(upPositionX, upPositionY)){
                    seizureModeStateText = "OFF";
                    setViewTo("settings");
                }

                break;
        }
    }

    private void setNonePressed() {
        okayOptionPressed = false;
        cancelOptionPressed = false;
    }

}
