package com.structurr.pong;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Sam on 6/24/2014.
 */
public class Preferences {

    Context currentContext;

    public void putString(String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void putInt(String key, int value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public String getString(String key, String defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentContext);
        String returnValue = preferences.getString(key, defaultValue);
        return returnValue;
    }

    public int getInt(String key, int defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentContext);
        int returnValue = preferences.getInt(key, defaultValue);
        return returnValue;
    }

    public void setContext(Context context) {
        currentContext = context;
    }

}
