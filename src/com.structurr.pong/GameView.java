package com.structurr.pong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.util.TypedValue;
import android.view.View;

import java.util.Random;

/**
 * Created by Sam on 6/2/2014.
 */
public class GameView extends MainActivity { // Draws the game

    public static class Draw extends View {

        Context currentContext;

        boolean firstDraw = true;

        Paint gamePaint = new Paint(); // Used for pads and ball
        Paint playgroundPaint = new Paint(); // Used for scores and text

        final String restartText = "TOUCH TO START";

        Pads pads = new Pads(); // Instance of Pads class to calculate pad position
        Ball ball = new Ball(); // Instance of Ball class to calculate ball position

        public Draw(Context context) {
            super(context);

            currentContext = context;
        }

        @Override
        public void onDraw(Canvas canvas) {
            changeStatusBarColor();

            if (firstDraw) { // Used to prevent commands from unnecessarily being repeated
                gamePaint.setColor(gameColor);

                firstDraw = false;
            }

            if (isRunning) { // While game is running, calculate pads and ball positions
                pads.process();
                ball.process();
            }

            if (seizureModeStateText != "OFF") {
                MainActivity.generateColorsForSeizureMode();
            }

            drawPlayground(canvas);
            drawPads(canvas);
            drawBall(canvas);

            drawScores(canvas);

            invalidate(); // Draw everything again
        }

        private void drawPlayground(Canvas canvas) { // Draw any elements that are considered background
            playgroundPaint.setColor(gameColor);
            playgroundPaint.setAlpha(35);

            canvas.drawColor(backColor);

            if (isRunning) { // If game is running, draw partition
                Rect[] division = new Rect[12];
                int divisionHeight = (int) (padHeight * 0.6);
                int divisionWidth = (int) (canvasWidth * 0.03);
                int divisionPadding = (int) ((canvasWidth - (divisionWidth * 12)) / 13);

                int marker = divisionPadding;
                for (int n = 0; n < 12; n++) {
                    division[n] = new Rect();
                    division[n].set(marker, (canvasHeight / 2) - (divisionHeight / 2), marker + divisionWidth,
                            (canvasHeight / 2) + (divisionHeight / 2));
                    canvas.drawRect(division[n], playgroundPaint);
                    marker = marker + divisionWidth + divisionPadding;
                }
            } else if (!isRunning) { // If game is not running, draw restart text
                drawRestartText(canvas);
            }
        }

        private void drawPads(Canvas canvas) { // Draw pads
            Rect topPad = new Rect();
            topPad.set(topPadPos, 0 + padding, topPadPos + padWidth, 0 + padding + padHeight);
            canvas.drawRect(topPad, gamePaint);

            Rect bottomPad = new Rect();
            bottomPad.set(bottomPadPos, canvasHeight - padding - padHeight, bottomPadPos + padWidth, canvasHeight - padding);
            canvas.drawRect(bottomPad, gamePaint);
        }

        private void drawBall(Canvas canvas) { // Draw ball
            Rect ball = new Rect();
            ball.set(ballPosX, ballPosY, ballPosX + ballHeightWidth, ballPosY + ballHeightWidth);
            canvas.drawRect(ball, gamePaint);
        }

        public void drawScores(Canvas canvas) { // Draw scores
            int textDPSize = 150;
            float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    textDPSize, getResources().getDisplayMetrics()); // Convert regular text size to DP
                                                                        // for constant text size on all devices

            textPaint.setColor(gameColor);
            textPaint.setAlpha(75);
            textPaint.setTextSize(textSize);
            textPaint.setTypeface(customFace); // Use custom font

            // Puts topScoreBounds and bottomScoreBounds as enclosing rectangles
                // to determine the space taken up by the score
            Rect topScoreBounds = new Rect();
            Rect bottomScoreBounds = new Rect();
            textPaint.getTextBounds("" + topPlayerScore, 0, ("" + topPlayerScore).length(), topScoreBounds);
            textPaint.getTextBounds("" + bottomPlayerScore, 0, ("" + bottomPlayerScore).length(), bottomScoreBounds);

            canvas.drawText("" + bottomPlayerScore, (float) ((canvasWidth * 0.5) - (bottomScoreBounds.width() / 2)), (float) (canvasHeight * 0.75), textPaint);

            /*// Flip canvas, then draw text, and flip it back for upside down score
            canvas.scale(-1, -1, canvasWidth / 2, canvasHeight / 2);
            canvas.drawText("" + topPlayerScore, (float) ((canvasWidth * 0.5) - (topScoreBounds.width() / 2)), (float) (canvasHeight * 0.75), textPaint);
            canvas.scale(-1, -1, canvasWidth / 2, canvasHeight / 2);*/
            canvas.drawText("" + topPlayerScore, (float) ((canvasWidth * 0.5) - (topScoreBounds.width() / 2)), (float) (canvasHeight * 0.25) + topScoreBounds.height(), textPaint);
        }

        public void drawRestartText(Canvas canvas) { // Draw restart text / Ask user to restart
            int textDPSize = 150;
            float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    textDPSize, getResources().getDisplayMetrics());

            textPaint.setColor(gameColor);
            textPaint.setAlpha(75);
            textPaint.setTextSize(textSize);
            textPaint.setTypeface(customFace); // Use custom font

            // Puts restartTextBounds as an enclosing rectangle to determine the space taken up by the restart text
            Rect restartTextBounds = new Rect();
            textPaint.getTextBounds(restartText, 0, restartText.length(), restartTextBounds);

            // While width of restart text is less than 75% of canvas, increase text size
            while (restartTextBounds.width() < (canvasWidth * 0.75)) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(restartText, 0, restartText.length(), restartTextBounds);
            }

            // While width of restart text is greater than 80% of canvas, decrease text size
            while (restartTextBounds.width() > (canvasWidth * 0.80)) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(restartText, 0, restartText.length(), restartTextBounds);
            }

            canvas.drawText(restartText, (canvasWidth / 2) - (restartTextBounds.width() / 2),
                    (canvasHeight / 2) + (restartTextBounds.height() / 2), textPaint);
        }

        private void generateColorsForSeizureMode() {
            Random random = new Random();

            gameColor = random.nextInt(16777316) * -1;
            backColor = random.nextInt(16777316) * -1;
        }

    }

}