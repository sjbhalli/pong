package com.structurr.pong;

import android.app.Notification;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Sam on 6/22/2014.
 */
public class SettingsView extends MainActivity {

    static Rect gameColorChangeOptionFinalBounds = new Rect();
    static Rect backColorChangeOptionFinalBounds = new Rect();
    static Rect seizureModeToggleFinalBounds = new Rect();
    static Rect audioOptionFinalBounds = new Rect();
    static Rect hapticOptionFinalBounds = new Rect();
    static Rect initialSpeedChangeOptionFinalBounds = new Rect();
    static Rect resetButtonFinalBounds = new Rect();

    static boolean gameColorOptionPressed = false;
    static boolean backColorOptionPressed = false;
    static boolean seizureModeOptionPressed = false;
    static boolean audioOptionPressed = false;
    static boolean hapticOptionPressed = false;
    static boolean initialBallSpeedOptionPressed = false;
    static boolean resetButtonPressed = false;


    public static class Draw extends View {

        int textSize = 1;

        String settingsTitleText = "SETTINGS";
        String gameColorChangeText = "GAME COLOR";
        String backColorChangeText = "BACKGROUND COLOR";
        String seizureModeOptionText = "SEIZURE MODE: ";
        String audioOptionText = "AUDIO: ";
        String hapticOptionText = "VIBRATION: ";
        String initialSpeedOptionText = "INITIAL BALL SPEED:";
        String resetOptionText = "RESET";

        Rect settingsTitleBounds = new Rect();
        Rect gameColorChangeBounds = new Rect();
        Rect backColorChangeBounds = new Rect();
        Rect seizureModeToggleBounds = new Rect();
        Rect audioOptionBounds = new Rect();
        Rect hapticOptionBounds = new Rect();
        Rect initialSpeedChangeBounds = new Rect();
        Rect initialBallSpeedPrefBounds = new Rect();
        Rect resetButtonBounds = new Rect();

        int settingsTitleTop = 0;
        int settingsTitleLeft = 0;
        
        int gameColorChangeTop = 0;
        int gameColorChangeLeft = 0;
        
        int backColorChangeTop = 0;
        int backColorChangeLeft = 0;
        
        int seizureModeToggleTop = 0;
        int seizureModeToggleLeft = 0;
        
        int audioOptionTop = 0;
        int audioOptionLeft = 0;

        int hapticOptionTop = 0;
        int hapticOptionLeft = 0;

        int initialSpeedChangeTop = 0;
        int initialSpeedChangeLeft = 0;
        int initialBallSpeedPrefTop = 0;
        int initialBallSpeedPrefLeft = 0;
        
        int resetButtonTop = 0;
        int resetButtonLeft = 0;

        Preferences preferences = new Preferences();

        public Draw(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            changeStatusBarColor();

            if (seizureModeStateText != "OFF") {
                generateColorsForSeizureMode();
            }

            textPaint.setTextSize(textSize);
            textPaint.setTypeface(customFace);
            textPaint.setColor(gameColor);

            drawBackground(canvas);
            drawTitle(canvas);
            drawDisplaySettings(canvas);
            drawAudioSettings(canvas);
            drawHapticSettings(canvas);
            drawGamePlaySettings(canvas);
            drawResetButton(canvas);
            invalidate();
        }

        private void drawBackground(Canvas canvas) {
            canvas.drawColor(backColor);
        }

        private void drawTitle(Canvas canvas) {
            textPaint.getTextBounds(settingsTitleText, 0, settingsTitleText.length(), settingsTitleBounds);

            while (settingsTitleBounds.height() < canvasHeight * 0.05) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(settingsTitleText, 0, settingsTitleText.length(), settingsTitleBounds);
            }

            while (settingsTitleBounds.height() > canvasHeight * 0.06) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(settingsTitleText, 0, settingsTitleText.length(), settingsTitleBounds);
            }

            settingsTitleTop  = (int) (0 + (canvasHeight * 0.2) + (settingsTitleBounds.height() / 2));
            settingsTitleLeft = (int) (0 + (canvasWidth / 2) - (settingsTitleBounds.width() / 2));

            canvas.drawText(settingsTitleText, settingsTitleLeft, settingsTitleTop, textPaint);
        }

        private void drawDisplaySettings(Canvas canvas) {
            textPaint.getTextBounds(gameColorChangeText, 0, gameColorChangeText.length(), gameColorChangeBounds);

            while (gameColorChangeBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(gameColorChangeText, 0, gameColorChangeText.length(), gameColorChangeBounds);
            }

            while (gameColorChangeBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(gameColorChangeText, 0, gameColorChangeText.length(), gameColorChangeBounds);
            }

            textPaint.getTextBounds(backColorChangeText, 0, backColorChangeText.length(), backColorChangeBounds);

            while (backColorChangeBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(backColorChangeText, 0, backColorChangeText.length(), backColorChangeBounds);
            }

            while (backColorChangeBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(backColorChangeText, 0, backColorChangeText.length(), backColorChangeBounds);
            }

            textPaint.getTextBounds(seizureModeOptionText + seizureModeStateText, 0, (seizureModeOptionText + seizureModeStateText).length(), seizureModeToggleBounds);

            while (seizureModeToggleBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(seizureModeOptionText + seizureModeStateText, 0, (seizureModeOptionText + seizureModeStateText).length(), seizureModeToggleBounds);
            }

            while (seizureModeToggleBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(seizureModeOptionText + seizureModeStateText, 0, (seizureModeOptionText + seizureModeStateText).length(), seizureModeToggleBounds);
            }

            gameColorChangeTop = (int) (settingsTitleTop + (canvasHeight * 0.15));
            gameColorChangeLeft = (int) (0 + (canvasWidth / 2) - (gameColorChangeBounds.width() / 2));
            backColorChangeTop = (int) (gameColorChangeTop + gameColorChangeBounds.height() + backColorChangeBounds.height());
            backColorChangeLeft = (int) (0 + (canvasWidth / 2) - (backColorChangeBounds.width() / 2));
            seizureModeToggleTop = (int) (backColorChangeTop + backColorChangeBounds.height() + seizureModeToggleBounds.height());
            seizureModeToggleLeft = (int) (0 + (canvasWidth / 2) - (seizureModeToggleBounds.width() / 2));

            if (gameColorOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(gameColorChangeText, gameColorChangeLeft, gameColorChangeTop, textPaint);
            textPaint.setAlpha(255);

            if (backColorOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(backColorChangeText, backColorChangeLeft, backColorChangeTop, textPaint);
            textPaint.setAlpha(255);

            if (seizureModeOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(seizureModeOptionText + seizureModeStateText, seizureModeToggleLeft, seizureModeToggleTop, textPaint);
            textPaint.setAlpha(255);
        }

        private void drawAudioSettings(Canvas canvas) {
            textPaint.getTextBounds(audioOptionText + audioOptionStateText, 0, (audioOptionText + audioOptionStateText).length(), audioOptionBounds);

            while (audioOptionBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(audioOptionText + audioOptionStateText, 0, (audioOptionText + audioOptionStateText).length(), audioOptionBounds);
            }

            while (audioOptionBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(audioOptionText + audioOptionStateText, 0, (audioOptionText + audioOptionStateText).length(), audioOptionBounds);
            }

            audioOptionTop = seizureModeToggleTop + (2 * audioOptionBounds.height()) + audioOptionBounds.height();
            audioOptionLeft = (int) (0 + (canvasWidth / 2) - (audioOptionBounds.width() / 2));

            if (audioOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(audioOptionText + audioOptionStateText, audioOptionLeft, audioOptionTop, textPaint);
            textPaint.setAlpha(255);
        }

        private void drawHapticSettings(Canvas canvas) {
            textPaint.getTextBounds(hapticOptionText + hapticOptionStateText, 0, (hapticOptionText + hapticOptionStateText).length(), hapticOptionBounds);

            while (hapticOptionBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(hapticOptionText + hapticOptionStateText, 0, (hapticOptionText + hapticOptionStateText).length(), hapticOptionBounds);
            }

            while (hapticOptionBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(hapticOptionText + hapticOptionStateText, 0, (hapticOptionText + hapticOptionStateText).length(), hapticOptionBounds);
            }

            hapticOptionTop = audioOptionTop + (2 * hapticOptionBounds.height());
            hapticOptionLeft = (int) (0 + (canvasWidth / 2) - (hapticOptionBounds.width() / 2));

            if (hapticOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(hapticOptionText + hapticOptionStateText, hapticOptionLeft, hapticOptionTop, textPaint);
            textPaint.setAlpha(255);
        }

        private void drawGamePlaySettings(Canvas canvas) {
            textPaint.getTextBounds(initialSpeedOptionText, 0, initialSpeedOptionText.length(), initialSpeedChangeBounds);

            while (initialSpeedChangeBounds.height() < canvasHeight * 0.02) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(initialSpeedOptionText, 0, initialSpeedOptionText.length(), initialSpeedChangeBounds);
            }

            while (initialSpeedChangeBounds.height() > canvasHeight * 0.0225) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(initialSpeedOptionText, 0, initialSpeedOptionText.length(), initialSpeedChangeBounds);
            }

            initialSpeedChangeTop = hapticOptionTop + (2 * initialSpeedChangeBounds.height()) + initialSpeedChangeBounds.height();
            initialSpeedChangeLeft = (int) (0 + (canvasWidth / 2) - (initialSpeedChangeBounds.width() / 2));

            if (initialBallSpeedOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(initialSpeedOptionText, initialSpeedChangeLeft, initialSpeedChangeTop, textPaint);
            textPaint.setAlpha(255);

            textPaint.getTextBounds(initialBallSpeedPrefText, 0, initialBallSpeedPrefText.length(), initialBallSpeedPrefBounds);

            while (initialBallSpeedPrefBounds.height() < canvasHeight * 0.0125) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(initialBallSpeedPrefText, 0, initialBallSpeedPrefText.length(), initialBallSpeedPrefBounds);
            }

            while (initialBallSpeedPrefBounds.height() > canvasHeight * 0.015) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(initialBallSpeedPrefText, 0, initialBallSpeedPrefText.length(), initialBallSpeedPrefBounds);
            }

            initialBallSpeedPrefTop = initialSpeedChangeTop + (initialBallSpeedPrefBounds.height() / 2) + initialBallSpeedPrefBounds.height();
            initialBallSpeedPrefLeft = (int) (0 + (canvasWidth / 2) - (initialBallSpeedPrefBounds.width() / 2));

            if (initialBallSpeedOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(initialBallSpeedPrefText, initialBallSpeedPrefLeft, initialBallSpeedPrefTop, textPaint);
            textPaint.setAlpha(255);
        }

        private void drawResetButton(Canvas canvas) {
            textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);

            while (resetButtonBounds.height() < canvasHeight * 0.0125) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);
            }

            while (resetButtonBounds.height() > canvasHeight * 0.015) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(resetOptionText, 0, resetOptionText.length(), resetButtonBounds);
            }

            resetButtonTop = (int) canvasHeight - (resetButtonBounds.height() / 2) - resetButtonBounds.height();
            resetButtonLeft  = (int) canvasWidth - resetButtonBounds.height() - resetButtonBounds.width();

            if (backColor > -1000000) {
                textPaint.setColor(Color.BLACK);
            } else if (backColor < -15777216) {
                textPaint.setColor(Color.WHITE);
            }

            if (resetButtonPressed) textPaint.setAlpha(50);
            canvas.drawText(resetOptionText, resetButtonLeft, resetButtonTop, textPaint);
            textPaint.setAlpha(255);

            setFinalOptionBounds();
            //TODO do only once
        }

        private void setFinalOptionBounds() {
            gameColorChangeOptionFinalBounds.left = gameColorChangeLeft - (gameColorChangeBounds.height() / 2);
            gameColorChangeOptionFinalBounds.top = gameColorChangeTop;
            gameColorChangeOptionFinalBounds.right = gameColorChangeLeft + gameColorChangeBounds.width() + (gameColorChangeBounds.height() / 2);
            gameColorChangeOptionFinalBounds.bottom = gameColorChangeTop + gameColorChangeBounds.height() + (gameColorChangeBounds.height() * 3 / 2);

            backColorChangeOptionFinalBounds.left = backColorChangeLeft - (backColorChangeBounds.height() / 2);
            backColorChangeOptionFinalBounds.top = backColorChangeTop;
            backColorChangeOptionFinalBounds.right = backColorChangeLeft + backColorChangeBounds.width() + (backColorChangeBounds.height() / 2);
            backColorChangeOptionFinalBounds.bottom = backColorChangeTop + backColorChangeBounds.height() + (backColorChangeBounds.height() * 3 / 2);

            seizureModeToggleFinalBounds.left = seizureModeToggleLeft - (seizureModeToggleBounds.height() / 2);
            seizureModeToggleFinalBounds.top = seizureModeToggleTop;
            seizureModeToggleFinalBounds.right = seizureModeToggleLeft + seizureModeToggleBounds.width() + (seizureModeToggleBounds.height() / 2);
            seizureModeToggleFinalBounds.bottom = seizureModeToggleTop + seizureModeToggleBounds.height() + (seizureModeToggleBounds.height() * 3 / 2);

            audioOptionFinalBounds.left = audioOptionLeft - (audioOptionBounds.height() / 2);
            audioOptionFinalBounds.top = audioOptionTop;
            audioOptionFinalBounds.right = audioOptionLeft + audioOptionBounds.width() + (audioOptionBounds.height() / 2);
            audioOptionFinalBounds.bottom = audioOptionTop + audioOptionBounds.height() + (audioOptionBounds.height() * 3 / 2);

            hapticOptionFinalBounds.left = hapticOptionLeft - (hapticOptionBounds.height() / 2);
            hapticOptionFinalBounds.top = hapticOptionTop;
            hapticOptionFinalBounds.right = hapticOptionLeft + hapticOptionBounds.width() + (hapticOptionBounds.height() / 2);
            hapticOptionFinalBounds.bottom = hapticOptionTop + hapticOptionBounds.height() + (hapticOptionBounds.height() * 3 / 2);
            
            initialSpeedChangeOptionFinalBounds.left = initialSpeedChangeLeft - (initialSpeedChangeBounds.height() / 2);
            initialSpeedChangeOptionFinalBounds.top = initialSpeedChangeTop;
            initialSpeedChangeOptionFinalBounds.right = initialSpeedChangeLeft + initialSpeedChangeBounds.width() + (initialSpeedChangeBounds.height() / 2);
            initialSpeedChangeOptionFinalBounds.bottom = initialSpeedChangeTop + initialSpeedChangeBounds.height() +
                    (initialBallSpeedPrefBounds.height() / 2) + initialBallSpeedPrefBounds.height() + (initialSpeedChangeBounds.height() * 3 / 2);

            resetButtonFinalBounds.left = resetButtonLeft - (resetButtonBounds.height() / 2);
            resetButtonFinalBounds.top = resetButtonTop;
            resetButtonFinalBounds.right = resetButtonLeft + resetButtonBounds.width() + (resetButtonBounds.height() / 2);
            resetButtonFinalBounds.bottom = 2 * canvasHeight;
        }

    }

}
