package com.structurr.pong;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Sam on 6/6/2014.
 */
public class IntroTouch extends IntroView {

    static int downPositionX = 0;
    static int downPositionY = 0;
    static int movePositionX = 0;
    static int movePositionY = 0;
    static int upPositionX = 0;
    static int upPositionY = 0;

    static Context currentContext;

    public static void process(MotionEvent motionEvent) {
        Preferences preferences = new Preferences();

        int motionAction = (motionEvent.getAction() & MotionEvent.ACTION_MASK);

        switch (motionAction) {
            case MotionEvent.ACTION_DOWN:
                downPositionX = (int) motionEvent.getX();
                downPositionY = (int) motionEvent.getY();

                if (easyOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    easyOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (normalOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    normalOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (hardOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hardOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (intellidroidOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    intellidroidOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (settingsOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    settingsOptionPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier);
                } else if (resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                    resetButtonPressed = true;
                    vibrator.vibrate(buttonTouchVibrateDuration * hapticMultiplier * 2);
                }

                if (!easyOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    easyOptionPressed = false;
                }
                if (!normalOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    normalOptionPressed = false;
                }
                if (!hardOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hardOptionPressed = false;
                }
                if (!intellidroidOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    intellidroidOptionPressed = false;
                }
                if (!settingsOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    settingsOptionPressed = false;
                }
                if (!resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                resetButtonPressed = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                movePositionX = (int) motionEvent.getX();
                movePositionY = (int) motionEvent.getY();

                if (easyOptionFinalBounds.contains(movePositionX, movePositionY) && easyOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    easyOptionPressed = true;
                } else if (normalOptionFinalBounds.contains(movePositionX, movePositionY) && normalOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    normalOptionPressed = true;
                } else if (hardOptionFinalBounds.contains(movePositionX, movePositionY) && hardOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hardOptionPressed = true;
                } else if (intellidroidOptionFinalBounds.contains(movePositionX, movePositionY) && intellidroidOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    intellidroidOptionPressed = true;
                } else if (settingsOptionFinalBounds.contains(movePositionX, movePositionY) && settingsOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    settingsOptionPressed = true;
                } else if (resetButtonFinalBounds.contains(movePositionX, movePositionY) && resetButtonFinalBounds.contains(downPositionX, downPositionY)) {
                    resetButtonPressed = true;
                }

                if (!easyOptionFinalBounds.contains(movePositionX, movePositionY) && easyOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    easyOptionPressed = false;
                }
                if (!normalOptionFinalBounds.contains(movePositionX, movePositionY) && normalOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    normalOptionPressed = false;
                }
                if (!hardOptionFinalBounds.contains(movePositionX, movePositionY) && hardOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    hardOptionPressed = false;
                }
                if (!intellidroidOptionFinalBounds.contains(movePositionX, movePositionY) && intellidroidOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    intellidroidOptionPressed = false;
                }
                if (!settingsOptionFinalBounds.contains(movePositionX, movePositionY) && settingsOptionFinalBounds.contains(downPositionX, downPositionY)) {
                    settingsOptionPressed = false;
                }
                if (!resetButtonFinalBounds.contains(movePositionX, movePositionY)) {
                    resetButtonPressed = false;
                }
                break;
            case MotionEvent.ACTION_UP:
                upPositionX = (int) motionEvent.getX();
                upPositionY = (int) motionEvent.getY();

                setNonePressed();

                if (easyOptionFinalBounds.contains(downPositionX, downPositionY) && easyOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    AIDifficultyString = "easy";
                    Pads.setAIDifficulty();
                    toast.setText("Easy");
                    toast.show();
                    speedIncreaseInterval = 3;
                    atIntro = false;
                    atGame = true;
                } else if (normalOptionFinalBounds.contains(downPositionX, downPositionY) && normalOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    AIDifficultyString = "normal";
                    Pads.setAIDifficulty();
                    toast.setText("Normal");
                    toast.show();
                    speedIncreaseInterval = 3;
                    atIntro = false;
                    atGame = true;
                } else if (hardOptionFinalBounds.contains(downPositionX, downPositionY) && hardOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    AIDifficultyString = "hard";
                    Pads.setAIDifficulty();
                    toast.setText("Hard");
                    toast.show();
                    speedIncreaseInterval = 2;
                    atIntro = false;
                    atGame = true;
                } else if (intellidroidOptionFinalBounds.contains(downPositionX, downPositionY) && intellidroidOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    toast.setText("Intellidroid");
                    toast.show();
                    AIDifficulty = 2;
                    speedIncreaseInterval = 2;
                    AIDifficultyString = "intellidroid";
                    atIntro = false;
                    atGame = true;
                } else if (settingsOptionFinalBounds.contains(downPositionX, downPositionY) && settingsOptionFinalBounds.contains(upPositionX, upPositionY)) {
                    atIntro = false;
                    atSettings = true;
                } else if (resetButtonFinalBounds.contains(downPositionX, downPositionY) && resetButtonFinalBounds.contains(upPositionX, upPositionY)) {
                    preferences.setContext(currentContext);
                    preferences.putInt("gameColorPref", -1);
                    preferences.putInt("chosenGameColorPref", -1);
                    preferences.putInt("backColorPref", -16777216);
                    preferences.putInt("chosenBackColorPref", -16777216);
                    gameColor = -1;
                    backColor = -16777216;
                    seizureModeStateText = "OFF";
                    audioOptionStateText = "BOP";
                    hapticOptionStateText = "LOW";
                    hapticMultiplier = 0;
                    initialBallSpeedPrefText = "NORMAL";
                    initialBallSpeedDifference = 3;
                    preferences.putString("audioOptionState", audioOptionStateText);
                    preferences.putString("initialBallSpeedPrefText", "NORMAL");
                    preferences.putInt("initialBallSpeedDifference", 3);
                    toast.setText("Preferences reset");
                    toast.show();
                }
                Log.d(AIDifficultyString, AIDifficultyString);
                break;
        }
    }

    private static void setNonePressed() {
        easyOptionPressed = false;
        normalOptionPressed = false;
        hardOptionPressed = false;
        intellidroidOptionPressed = false;
        settingsOptionPressed = false;
        resetButtonPressed = false;
    }

    public void setContext(Context context) {
        currentContext = context;
    }

}
