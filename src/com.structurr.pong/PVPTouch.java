package com.structurr.pong;

import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Sam on 6/2/2014.
 */
public class PVPTouch extends GameView {

    public static void process(MotionEvent motionEvent) {

        Pads pads = new Pads();

        final int MAX_TOUCH_COUNT = 10;
        int[] x = new int[MAX_TOUCH_COUNT];
        int[] y = new int[MAX_TOUCH_COUNT];

        int pointerIndex = ((motionEvent.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT);
        int pointerId = motionEvent.getPointerId(pointerIndex);
        int motionAction = (motionEvent.getAction() & MotionEvent.ACTION_MASK);
        int pointCount = motionEvent.getPointerCount();

        for (int i = 0; i < pointCount && i < MAX_TOUCH_COUNT; i++) {
            int id = motionEvent.getPointerId(i);
            x[id] = (int) motionEvent.getX(i);
            y[id] = (int) motionEvent.getY(i);
        }

//        if (isRunning) {
//            switch (motionAction) {
//                case MotionEvent.ACTION_DOWN:
//                        topPadPos = x[pointerId];
//                        Log.d("Top", "action_down");
////                    break;
//                case MotionEvent.ACTION_POINTER_DOWN:
//                        topPadPos = x[pointerId];
//                        Log.d("Top", "action_pointer_down");
////                    break;
//                case MotionEvent.ACTION_UP:
//                        topPlayerMoving = false;
//                        Log.d("Top", "action_up");
////                    break;
//                case MotionEvent.ACTION_POINTER_UP:
//                        topPlayerMoving = false;
//                        Log.d("Top", "action_pointer_up");
////                    break;
//                case MotionEvent.ACTION_MOVE:
//                        topPadPos = x[pointerId];
//                        Log.d("Top", "action_move");
////                    break;
//            }
//        }

        if (isRunning) {
            switch (motionAction) {
                case MotionEvent.ACTION_DOWN:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        initialTPTop = x[pointerId];
                        topPadPosOnDown = topPadPos;
                        topPlayerMoving = true;
                        Log.d("Top", "action_down");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        initialTPBottom = x[pointerId];
                        bottomPadPosOnDown = bottomPadPos;
                        bottomPlayerMoving = true;
                        Log.d("Bottom", "action_down");
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        initialTPTop = x[pointerId];
                        topPadPosOnDown = topPadPos;
                        topPlayerMoving = true;
                        Log.d("Top", "action_pointer_down");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        initialTPBottom = x[pointerId];
                        bottomPadPosOnDown = bottomPadPos;
                        bottomPlayerMoving = true;
                        Log.d("Bottom", "action_pointer_down");
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        topPlayerMoving = false;
                        Log.d("Top", "action_up");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        bottomPlayerMoving = false;
                        Log.d("Bottom", "action_up");
                    }
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    if (y[pointerId] < (canvasHeight / 2)) {
                        topPlayerMoving = false;
                        Log.d("Top", "action_pointer_up");
                    } if (y[pointerId] > (canvasHeight / 2)) {
                        bottomPlayerMoving = false;
                        Log.d("Bottom", "action_pointer_up");
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    for (int n = 0; n < pointCount; n++) {
                        if (y[n] < (canvasHeight / 2)) {
                            currTPTop = x[n];
                            topPlayerMoving = true;
                            pads.process();
                            Log.d("Top", "action_move");
                        } if (y[n] > (canvasHeight / 2)) {
                            currTPBottom = x[n];
                            bottomPlayerMoving = true;
                            pads.process();
                            Log.d("Bottom", "action_move");
                        }
                    }
                    break;
            }
        }
    }

}
