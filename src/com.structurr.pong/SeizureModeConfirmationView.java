package com.structurr.pong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Sam on 6/27/2014.
 */
public class SeizureModeConfirmationView extends MainActivity {

    static Rect okayOptionFinalBounds = new Rect();
    static Rect cancelOptionFinalBounds = new Rect();

    static boolean okayOptionPressed = false;
    static boolean cancelOptionPressed = false;

    public static class Draw extends View {

        int textSize = 1;

        final String titleText = "SEIZURE MODE";
        final String okayText = "OKAY";
        final String cancelText = "CANCEL";
        String[] warningText = new String[7];

        Rect titleTextBounds = new Rect();
        Rect warningTextBounds = new Rect();
        Rect okayTextBounds = new Rect();
        Rect cancelTextBounds = new Rect();

        int titleTop = 0;
        int titleLeft = 0;
        int[] warningTop = new int[7];
        int[] warningLeft = new int[7];
        int okayTop = 0;
        int okayLeft = 0;
        int cancelTop = 0;
        int cancelLeft = 0;

        public Draw(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            changeStatusBarColor();

            warningText[0] = "WARNING";
            warningText[1] = "PLEASE NOTE THAT SEIZURE MODE";
            warningText[2] = "MAY INDUCE SEVERE SEIZURES. IF";
            warningText[3] = "YOU HAVE EXPERIENCED AND/OR ARE";
            warningText[4] = "AFFLICTED WITH EPILEPSY OR ANY";
            warningText[5] = "OTHER TYPE OF SEIZURE INDUCING";
            warningText[6] = "DISORDER, PLEASE PRESS CANCEL.";

            canvas.drawColor(Color.BLACK);

            textPaint.setColor(gameColor);

            drawTitle(canvas);
            drawWarning(canvas);
            drawOkay(canvas);
            drawCancel(canvas);

            invalidate();
        }

        private void drawTitle(Canvas canvas) {
            textPaint.getTextBounds(titleText, 0, titleText.length(), titleTextBounds);

            while (titleTextBounds.height() < canvasHeight * 0.04) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(titleText, 0, titleText.length(), titleTextBounds);
            }

            while (titleTextBounds.height() > canvasHeight * 0.05) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(titleText, 0, titleText.length(), titleTextBounds);
            }

            titleTop = (int) ((canvasHeight * 0.2) + (titleTextBounds.height() / 2));
            titleLeft = (int) ((canvasWidth / 2) - (titleTextBounds.width() / 2));

            canvas.drawText(titleText, titleLeft, titleTop, textPaint);
        }

        private void drawWarning(Canvas canvas) {
            textPaint.getTextBounds(warningText[0], 0, warningText[0].length(), warningTextBounds);

            while (warningTextBounds.height() < canvasHeight * 0.015) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(warningText[0], 0, warningText[0].length(), warningTextBounds);
            }

            while (warningTextBounds.height() > canvasHeight * 0.0175) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(warningText[0], 0, warningText[0].length(), warningTextBounds);
            }

            warningTop[0] = (int) ((titleTop + titleTextBounds.height()) + (titleTextBounds.height() / 2));
            warningLeft[0] = (int) ((canvasWidth / 2) - (warningTextBounds.width() / 2));

            canvas.drawText(warningText[0], warningLeft[0], warningTop[0], textPaint);

            for (int n = 1; n <= warningTop.length - 1; n++) {
                textPaint.getTextBounds(warningText[n], 0, warningText[n].length(), warningTextBounds);

                warningTop[n] = (int) (warningTop[n - 1] + (warningTextBounds.height() * 2));
                warningLeft[n] = (int) ((canvasWidth / 2) - (warningTextBounds.width() / 2));

                canvas.drawText(warningText[n], warningLeft[n], warningTop[n], textPaint);
            }
        }

        private void drawOkay(Canvas canvas) {
            textPaint.getTextBounds(okayText, 0, okayText.length(), okayTextBounds);

            while (okayTextBounds.height() < canvasHeight * 0.0175) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(okayText, 0, okayText.length(), okayTextBounds);
            }

            while (okayTextBounds.height() > canvasHeight * 0.02) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(okayText, 0, okayText.length(), okayTextBounds);
            }

            okayTop = (int) (warningTop[warningTop.length - 1] + (6 * okayTextBounds.height()));
            okayLeft = (int) ((canvasWidth / 2) - (2 * okayTextBounds.width()) + (okayTextBounds.width() / 2));

            if (okayOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(okayText, okayLeft, okayTop, textPaint);
            textPaint.setAlpha(255);
        }

        private void drawCancel(Canvas canvas) {
            textPaint.getTextBounds(cancelText, 0, cancelText.length(), cancelTextBounds);

            while (cancelTextBounds.height() < canvasHeight * 0.0175) {
                textSize++;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(cancelText, 0, cancelText.length(), cancelTextBounds);
            }

            while (cancelTextBounds.height() > canvasHeight * 0.02) {
                textSize--;
                textPaint.setTextSize(textSize);

                textPaint.getTextBounds(cancelText, 0, cancelText.length(), cancelTextBounds);
            }

            cancelTop = okayTop;
            cancelLeft = (int) ((canvasWidth / 2) + okayTextBounds.width() - (cancelTextBounds.width() / 2));

            if (cancelOptionPressed) textPaint.setAlpha(50);
            canvas.drawText(cancelText, cancelLeft, cancelTop, textPaint);
            textPaint.setAlpha(255);

            setFinalOptionBounds();
        }

        private void setFinalOptionBounds() {
            okayOptionFinalBounds.left = (int) (okayLeft - okayTextBounds.height());
            okayOptionFinalBounds.top = (int) (okayTop - okayTextBounds.height());
            okayOptionFinalBounds.right = (int) (okayLeft + okayTextBounds.width() +  okayTextBounds.height());
            okayOptionFinalBounds.bottom = (int) (okayTop + okayTextBounds.height() + okayTextBounds.height());

            cancelOptionFinalBounds.left = (int) (cancelLeft - cancelTextBounds.height());
            cancelOptionFinalBounds.top = (int) (cancelTop - cancelTextBounds.height());
            cancelOptionFinalBounds.right = (int) (cancelLeft + cancelTextBounds.width() +  cancelTextBounds.height());
            cancelOptionFinalBounds.bottom = (int) (cancelTop + cancelTextBounds.height() + cancelTextBounds.height());
        }

    }

}
