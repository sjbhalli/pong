package com.structurr.pong;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.*;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by Sam on 5/31/2014.
 */

public class MainActivity extends Activity { // Incorporates all other classes,
    // handles global static variables

    static Paint textPaint = new Paint();
    static Typeface customFace; // Uses custom font

    static int gameColor = -1;
    static int backColor = -16777216;

    static boolean atIntro = true;
    static boolean atSettings = false;
    static boolean atSeizureConfirmation = false;
    static boolean atGame = false;
    static boolean isRunning = false;

    static String gameMode = "PVA";

    static int canvasHeight;
    static int canvasWidth;
    static int padding;

    static int padHeight;
    static int padWidth;
    static int ballHeightWidth;

    static boolean topPlayerMoving = false;
    static boolean bottomPlayerMoving = false;
    static int initialTPTop;
    static int initialTPBottom;
    static int topPadPosOnDown;
    static int bottomPadPosOnDown;
    static int currTPTop;
    static int currTPBottom;

    static int topPadPos;
    static int bottomPadPos;

    static int ballPosX;
    static int ballPosY;
    static int ballSpeedX;
    static int ballSpeedY;

    static String seizureModeStateText = "OFF";

    static String audioOptionStateText = "BOP";
    static String hapticOptionStateText = "LOW";

    static Vibrator vibrator;
    static int buttonTouchVibrateDuration = 5;
    static int hapticMultiplier = 1;

    static int initialBallSpeedDifference = 3;
    static String initialBallSpeedPrefText = "NORMAL";

    static double AIDifficulty = 0;
    static String AIDifficultyString = "none";
    static int speedIncreaseInterval = 2;

    static int topPlayerScore = 0;
    static int bottomPlayerScore = 0;

    static int winScore = 10;

    static SoundPool soundPool = new SoundPool(10,AudioManager.STREAM_MUSIC, 0);
    static int beepID;
    static int bopID;

    static Toast toast;

    static Window window;

    IntroTouch introTouch;
    SettingsTouch settingsTouch;
    SeizureModeConfirmationTouch seizureModeConfirmationTouch;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        window = getWindow();

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        beepID = soundPool.load(this, R.raw.beep, 1);
        bopID = soundPool.load(this, R.raw.bop, 1);

        vibrator = (Vibrator) this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

        introTouch = new IntroTouch();
        settingsTouch = new SettingsTouch();
        seizureModeConfirmationTouch = new SeizureModeConfirmationTouch();
        preferences = new Preferences();

        requestWindowFeature(Window.FEATURE_NO_TITLE); // Request application window with no title bar
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toast = new Toast(this);
        toast = Toast.makeText(this, "message", Toast.LENGTH_SHORT);

        getGameResources();

        setContentView(new IntroView.Draw(this)); // Show splash screen
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (atIntro) {
            introTouch.setContext(this);
            introTouch.process(motionEvent);
        }

        if (atSettings) {
            setContentView(new SettingsView.Draw(this));
            settingsTouch.setContext(this);
            settingsTouch.process(motionEvent);
        }

        if (atSeizureConfirmation) {
            setContentView(new SeizureModeConfirmationView.Draw(this));
            seizureModeConfirmationTouch.process(motionEvent);
        }

        if (atGame) {
            setContentView(new GameView.Draw(this));
        }

        if (gameMode == "PVP" && !atIntro && !atSettings && !atSeizureConfirmation) { // If game mode is player v. player, run PVP motion algorithm
            PVPTouch.process(motionEvent);
        } else if (gameMode == "PVA" && !atIntro && !atSettings && !atSeizureConfirmation) { // If game mode is player v. Android, run PVA motion algorithm
            PVATouch.process(motionEvent);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        menu.add("Pause"); // Add pause option to menu
//        menu.add("Start"); // Add start option to menu

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle() == "Pause") { // If pause item in menu clicked
            isRunning = false;
        } else if (item.getTitle() == "Start") { // If start item in menu clicked
            isRunning = true;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (atIntro) {
            exit();
        } else if (atGame) {
            setViewTo("intro");
        } else if (atSettings) {
            setViewTo("intro");
        } else if (atSeizureConfirmation) {
            setViewTo("settings");
        }
    }

    public void setViewTo(String view) {
        if (view == "intro") {
            atGame = false;
            atSettings = false;
            atSeizureConfirmation = false;
            atIntro = true;
            topPlayerScore = 0;
            bottomPlayerScore = 0;
            isRunning = false;
            AIDifficulty = 0;
            setContentView(new IntroView.Draw(MainActivity.this));
        } else if (view == "game") {
            atSettings = false;
            atIntro = false;
            atGame = true;
            setContentView(new GameView.Draw(MainActivity.this));
        } else if (view == "settings") {
            atGame = false;
            atIntro = false;
            atSeizureConfirmation = false;
            atSettings = true;
            setContentView(new SettingsView.Draw(MainActivity.this));
        } else if (view == "seizureModeConfirmation") {
            atSettings = false;
            atIntro = false;
            atGame = false;
            atSeizureConfirmation = true;
            setContentView(new SeizureModeConfirmationView.Draw(MainActivity.this));
        }
    }

    private void exit() {
        if (seizureModeStateText != "OFF") {
            preferences.setContext(this);
            gameColor = preferences.getInt("chosenGameColorPref", Color.rgb(255, 255, 255));
            backColor = preferences.getInt("chosenBackColorPref", Color.rgb(0, 0, 0));
            preferences.putInt("gameColorPref", gameColor);
            preferences.putInt("backColorPref", backColor);
            seizureModeStateText = "OFF";
        }
        finish();
    }

    public void getGameResources() { // Get custom font from assets and assign to custom TypeFace
        customFace = Typeface.createFromAsset(getAssets(), "fonts/bhalli_i.ttf");

        preferences.setContext(this);
        gameColor = preferences.getInt("chosenGameColorPref", Color.rgb(255, 255, 255));
        backColor = preferences.getInt("chosenBackColorPref", Color.rgb(0, 0, 0));
        audioOptionStateText = preferences.getString("audioOptionState", "BOP");

        hapticOptionStateText = preferences.getString("hapticOptionState", "LOW");
        if (hapticOptionStateText.equals("LOW")) hapticMultiplier = 1;
        else if (hapticOptionStateText.equals("HIGH")) hapticMultiplier = 2;
        else hapticMultiplier = 0;

        initialBallSpeedDifference = preferences.getInt("initialBallSpeedDifference", 3);
        initialBallSpeedPrefText = preferences.getString("initialBallSpeedPrefText", "NORMAL");
    }

    public static void generateColorsForSeizureMode() {
        Random rand = new Random();

        if (seizureModeStateText == "BW") {
            int backComponents = rand.nextInt(255);
            int gameComponents = rand.nextInt(255);

            backColor = Color.rgb(backComponents, backComponents, backComponents);
            gameColor = Color.rgb(gameComponents, gameComponents, gameComponents);
        } else if (seizureModeStateText == "FULL") {
            gameColor = Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
            backColor = Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
        }

        if (rand.nextInt(10) == 1)
            vibrator.vibrate(rand.nextInt(25));

        if (rand.nextInt(25) == 1)
            (new BallPadCollisionSoundPlayerTask()).execute();
    }

    public static void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int statusBarColor;

            int red = Color.red(backColor);
            int green = Color.green(backColor);
            int blue = Color.blue(backColor);

            if (red > 30) red -= 30; else red = 0;
            if (green > 30) green -= 30; else green = 0;
            if (blue > 30) blue -= 30; else blue = 0;

            statusBarColor = Color.rgb(red, green, blue);

            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(statusBarColor);
        };
    }

    static class BallPadCollisionSoundPlayerTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            if (!seizureModeStateText.equals("OFF"))
            {
                if ((new Random()).nextInt(2) == 0)
                    soundPool.play(bopID, 1f, 1f, 1, 0, 1f);
                else
                    soundPool.play(beepID, 1f, 1f, 1, 0, 1f);
            } else {
                if (audioOptionStateText.equals("BOP"))
                    soundPool.play(bopID, 1f, 1f, 1, 0, 1f);
                else if (audioOptionStateText.equals("BEEP"))
                    soundPool.play(beepID, 1f, 1f, 1, 0, 1f);
            }
            return null;
        }
    }

}