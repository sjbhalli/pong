package com.structurr.pong;

import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.util.Log;

import java.util.Random;

/**
 * Created by Sam on 5/31/2014.
 */
public class Pads extends MainActivity {

    public void process() {
        calcPos();

//        if (gameMode == "PVA" && AIDifficultyString == "intellidroid" && ballSpeedY > 0) {
//            returnToMiddle();
//        }
    }

    private void calcPos() { // Calculate pad positions
        if (gameMode == "PVP") {
            if (topPlayerMoving) {
                topPadPos = (topPadPosOnDown + (currTPTop - initialTPTop));
                checkForCollisions();
            }
        } else if (gameMode == "PVA") {
            setAIPadPos();
            checkForCollisions();
        }

        if (bottomPlayerMoving) {
            bottomPadPos = (bottomPadPosOnDown + (currTPBottom - initialTPBottom));
            checkForCollisions();
        }
    }

    private void returnToMiddle() {
        if ((topPadPos + (padWidth / 2)) > (canvasWidth / 2) + 5) {
            topPadPos -= Math.abs(ballSpeedX) * AIDifficulty;
        } else if ((topPadPos + (padWidth / 2)) < (canvasWidth / 2) - 5) {
            topPadPos += Math.abs(ballSpeedX) * AIDifficulty;
        }
    }

    private void setAIPadPos() { // Set pad position for AI
        if (AIDifficultyString == "intellidroid") {
//            if (topPadPos + (padWidth / 2) > ballPosX) {
//                topPadPos -= (2 * (Math.abs(ballSpeedX) * AIDifficulty));
//            } else if (topPadPos + (padWidth / 2) < ballPosX) {
//                topPadPos += (2 * (Math.abs(ballSpeedX) * AIDifficulty));
//            }
            topPadPos = ballPosX - (padWidth / 2);
            /*if ((topPadPos > ballPosX) && (topPadPos - padWidth < ballPosX)) {
                topPadPos -= (int) (2 * Math.ceil(Math.abs(ballSpeedX) * AIDifficulty));
            } else if ((topPadPos + padWidth < ballPosX) && (topPadPos + (2 * padWidth) > ballPosX)) {
                topPadPos += (int) (2 * Math.ceil(Math.abs(ballSpeedX) * AIDifficulty));
            } else if ((topPadPos > ballPosX) && (topPadPos - (2 * padWidth) > ballPosX)) {
                topPadPos -= (int) (2.5 * Math.ceil(Math.abs(ballSpeedX) * AIDifficulty));
            } else if ((topPadPos + padWidth < ballPosX) && (topPadPos + (3 * padWidth) < ballPosX)) {
                topPadPos += (int) (2.5 * Math.ceil(Math.abs(ballSpeedX) * AIDifficulty));
            }*/
        } else if (AIDifficultyString != "intellidroid") {
            if (topPadPos + (padWidth / 2) < ballPosX) {
                topPadPos += Math.ceil(Math.abs(ballSpeedX) * AIDifficulty);
            } else if (topPadPos + (padWidth / 2) > ballPosX) {
                topPadPos -= Math.ceil(Math.abs(ballSpeedX) * AIDifficulty);
            }
        }
    }

    private void checkForCollisions() { // Check for collisions between pads and canvas edges
        if (topPadPos < 0) {
            topPadPos = 0;
        } else if (topPadPos > (canvasWidth - padWidth)) {
            topPadPos = canvasWidth - padWidth;
        }

        if (bottomPadPos < 0) {
            bottomPadPos = 0;
        } else if (bottomPadPos > (canvasWidth - padWidth)) {
            bottomPadPos = canvasWidth - padWidth;
        }
    }

    public static void setAIDifficulty() {
        Random random = new Random();
        int randRange;
        if (AIDifficultyString.equals("easy")) {
            randRange = (90 - 60 + (bottomPlayerScore - 4 - topPlayerScore) * 5);
            double easyPadSpeed = random.nextInt(randRange < 1 ? 1 : randRange) + 60;

            easyPadSpeed = easyPadSpeed / 100;
            AIDifficulty = easyPadSpeed;
        } else if (AIDifficultyString.equals("normal")) {
            randRange = (100 - 70 + (bottomPlayerScore - 2 - topPlayerScore) * 10);
            double normalPadSpeed = random.nextInt(randRange < 1 ? 1 : randRange) + 70;
//            if (bottomPlayerScore - 3 >= topPlayerScore && normalPadSpeed < 100) normalPadSpeed = 100;

            normalPadSpeed = normalPadSpeed / 100;
            AIDifficulty = normalPadSpeed;
        } else if (AIDifficultyString.equals("hard")) {
            randRange = (100 - 70 + (bottomPlayerScore - topPlayerScore) * 10);
            double hardPadSpeed = random.nextInt(randRange < 1 ? 1 : randRange) + 70;

            if (bottomPlayerScore > winScore * 0.8  && topPlayerScore < winScore * 0.8 && hardPadSpeed < 100)
                hardPadSpeed = random.nextInt(randRange < 1 ? 1 : randRange) + 100;

            hardPadSpeed = hardPadSpeed / 100;
            AIDifficulty = hardPadSpeed;
        }
    }

//    public static void setNormalPadSpeed() {
//        Random random = new Random();
//        normalPadSpeed = random.nextInt((100 + (bottomPlayerScore - topPlayerScore) * 10) - (68 + (bottomPlayerScore - topPlayerScore) * 10)) + 68;
//
//        normalPadSpeed = normalPadSpeed / 100;
//        AIDifficulty = normalPadSpeed;
//    }

    public void resetPositions() {
        topPadPos = ((canvasWidth / 2) - (padWidth / 2));
        bottomPadPos = ((canvasWidth / 2) - (padWidth / 2));
    }

}