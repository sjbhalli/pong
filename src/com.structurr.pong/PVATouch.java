package com.structurr.pong;

import android.view.MotionEvent;

/**
 * Created by Sam on 6/2/2014.
 */
public class PVATouch extends GameView { // Processes touch in player v. Android mode

    public static void process(MotionEvent motionEvent) {

        Ball ball = new Ball(); // New instance of Ball class to calculate ball position

        final int MAX_TOUCH_COUNT = 10; // Amount of touches the game will read at one time
        int[] x = new int[MAX_TOUCH_COUNT]; // Array to store X positions of motion events
        int[] y = new int[MAX_TOUCH_COUNT]; // array to store Y position of motion events

        int pointerIndex = ((motionEvent.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT);
        int pointerId = motionEvent.getPointerId(pointerIndex);
        int motionAction = (motionEvent.getAction() & MotionEvent.ACTION_MASK);
        int pointCount = motionEvent.getPointerCount();

        // Get X and Y positions of all motion events
        for (int i = 0; i < pointCount && i < MAX_TOUCH_COUNT; i++) {
            int id = motionEvent.getPointerId(i);
            x[id] = (int) motionEvent.getX(i);
            y[id] = (int) motionEvent.getY(i);
        }

        // Set pad position to X position of touch
        if (isRunning) {
            switch (motionAction) {
                case MotionEvent.ACTION_DOWN:
                    bottomPadPos = x[pointerId] - (padWidth / 2);
                case MotionEvent.ACTION_POINTER_DOWN:
                    bottomPadPos = x[pointerId] - (padWidth / 2);
                case MotionEvent.ACTION_UP:
                    bottomPlayerMoving = false;
                case MotionEvent.ACTION_POINTER_UP:
                    bottomPlayerMoving = false;
                case MotionEvent.ACTION_MOVE:
                    bottomPadPos = x[pointerId] - (padWidth / 2);
            }
        } else if (!isRunning) {
            switch (motionAction) {
                case MotionEvent.ACTION_DOWN:
                    ball.reset();
                    Pads.setAIDifficulty();
                    isRunning = true;
                case MotionEvent.ACTION_POINTER_DOWN:
                    ball.reset();
                    Pads.setAIDifficulty();
                    isRunning = true;
            }
        }

    }

}
